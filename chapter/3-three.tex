%Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí
\chapter{Three External Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The kinematics of a scattering process with three external quanta are trivial in the sense that all physical quantities can be given as functions of the three masses. Still, it is a good way to illustrate the methods used in later chapters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Three Distinct Masses}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Any 3-point scattering process is inherently inelastic. Without loss of generality, I will consider a 2-to-1 scattering process. The most general 2-to-1 scattering process has the form:
\begin{equation}
	\phi_{1}(p_{1}) + \phi_{2}(p_{2}) \longrightarrow \psi_{3}(q_{3}).
	\label{AB2Z}
\end{equation}
In this section I will consider the case where each external quantum is slow. Thus, there are three on-shell constraints:
\begin{equation}
	p_{1}^{2} = m_{1}^{2}, \quad p_{2}^{2} = m_{2}^{2}, \quad q_{3}^{2} = m_{3}^{2}.
\end{equation}
You also have the conservation constraint:
\begin{equation}
	p_{1} + p_{2} = q_{3}.
\end{equation}
A good way to start is by making a list of all the available invariants.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Inventory of Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Besides the three masses, and functions of these masses, there is really nothing else to work with.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Mandelstam Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Due to the conservation constraint, every possible 2-Mandelstam invariant is equivalent to a mass:
\begin{equation}
	s_{12} = (p_{1} + p_{2})^{2} = m_{3}^{2}, \quad t_{13} = (p_{1} - q_{3})^{2} = m_{2}^{2}, \quad t_{23} = (p_{2} - q_{3})^{2} = m_{1}^{2}. 
\end{equation}
Since all masses are real, you have the conditions
\begin{equation}
	s_{12} > 0, \quad t_{13} > 0, \quad t_{23} > 0.
\end{equation}
Without loss of generality, I will use $\sqrt{s_{12}}$ instead of $m_{3}$.

The only possible 3-Mandelstam invariant is also trivial,
\begin{equation}
	t_{123} = (p_{1} + p_{2} - q_{3})^{2} = 0,
\end{equation}
because of the conservation constraint.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Regge Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
There are three possible Regge invariants:
\begin{equation}
	\sigma_{1 2} = \frac{s_{12} - m_{1}^{2} - m_{2}^{2}}{2 m_{1} m_{2}}, \quad \tau_{1 3} = - \frac{s_{12} - m_{2}^{2} + m_{1}^{2}}{2 m_{1} \sqrt{s_{12}}}, \quad \tau_{2 3} = -\frac{s_{12} - m_{1}^{2} + m_{2}^{2}}{2 m_{2} \sqrt{s_{12}}}.
\end{equation}
In some ways, only $\sigma_{1 2}$ is relevant for the (\ref{AB2Z}) process.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Gram Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
There are three possible 2-Gram invariants,
\begin{equation}
	G_{1 2} = \frac{1}{4} \Upsilon(s_{12}, m_{1}^{2}, m_{2}^{2}), \quad G_{1 3} = \frac{1}{4} \Upsilon(m_{2}^{2}, m_{1}^{2}, s_{12}), \quad G_{2 3} = \frac{1}{4} \Upsilon(m_{1}^{2}, m_{2}^{2}, s_{12}),
\end{equation}
where $\Upsilon$ is defined in (\ref{Kallen}). Due to the symmetry of $\Upsilon$, all 2-Gram invariants are equal.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Dual Spacetime Coordinates}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
With three external energy-momentum vectors, there is only one inequivalent sequence in the dual spacetime,
\begin{equation}
	123.
\end{equation}
Any other sequence will produce the same kinematics. You can introduce dual coordinates via:
\begin{equation}
	p_{1} = d_{\red{2}} - d_{\red{1}}, \quad p_{2} = d_{\red{3}} - d_{\red{2}}, \quad q_{3} = d_{\red{3}} - d_{\red{1}}.
\end{equation}
The \red{red} integers label points in the dual spacetime and the black integers label energy-momentum vectors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Simplex Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
You can use the three dual coordinates to define the positions of the vertices of a 2-simplex (a triangle). The area of this 2-simplex is related to the unique 3-point Cayley-Menger determinant,
\begin{equation}
	F_{\red{123}} = \det{ \begin{pmatrix}
	0 & d_{\red{12}}^{2} & d_{\red{13}}^{2} & 1 \\
	d_{\red{12}}^{2} & 0 & d_{\red{23}}^{2} & 1 \\
	d_{\red{13}}^{2} & d_{\red{23}}^{2} & 0 & 1 \\
	1          & 1          & 1 & 0
\end{pmatrix}	 } = \det{ \begin{pmatrix}
	0 & m_{1}^{2} & s_{12} & 1 \\
	m_{1}^{2} & 0 & m_{2}^{2} & 1 \\
	s_{12} & m_{2}^{2} & 0 & 1 \\
	1          & 1          & 1 & 0
\end{pmatrix}	 } = \Upsilon(s_{12}, m_{1}^{2}, m_{2}^{2}).
\end{equation}
Since the edges of this triangle are external energy-momentum vectors, the length of the edges are masses. The \textbf{euclidean area} of such a mass triangle is given by
\begin{equation}
	A_{\red{123}} = \frac{1}{4} \sqrt{-F_{\red{123}}} = \frac{1}{4} \sqrt{[s_{12} - (m_{1} - m_{2})^{2} ] [(m_{1} + m_{2})^{2} - s_{12}]}.
\end{equation}
In order for this area to be real, you must have $F_{\red{123}} < 0$, or equivalently,
\begin{equation}
	(m_{1} - m_{2})^{2} < s_{12} < (m_{1} + m_{2})^{2}.
\end{equation}
This interval is an example of a \textbf{2-body Coulomb-Regge window}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Dual Conformal Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Since you need the position of four distinct points in the dual spacetime, there are no possible dual conformal invariants.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Center-of-Momentum Frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In the \textbf{center-of-momentum frame}, you can write:
\begin{equation}
	p_{1} = \begin{pmatrix}
	E_{1} & \mathbf{p}_{1}
\end{pmatrix}, \quad p_{2} = \begin{pmatrix}
E_{2} & -\mathbf{p}_{1}
\end{pmatrix}, \quad q_{3} = \begin{pmatrix}
E_{3} & \mathbf{0}
\end{pmatrix}.
\end{equation}
From the on-shell constraint for $q_{3}$, it follows that
\begin{equation}
	E_{3} = \sqrt{s_{12}}.
\end{equation}
The other two on-shell constraints allow you to write
\begin{equation}
	E_{1} = \sqrt{m_{1}^{2} + \mathbf{p}_{1}^{2}}, \quad E_{2} = \sqrt{m_{2}^{2} + \mathbf{p}_{1}^{2}} \quad \Longrightarrow \quad E_{1}^{2} - m_{1}^{2} = E_{2}^{2} - m_{2}^{2}.
\end{equation}
The conservation of energy leads to the relation
\begin{equation}
	E_{2} = E_{3} - E_{1} = \sqrt{s_{12}} - E_{1}.
\end{equation}
From here it follows that the magnitude of the spatial momentum is given by
\begin{equation}
	\vert \mathbf{p}_{1} \vert = \frac{\sqrt{F_{\red{123}}}}{2 \sqrt{s_{12}}} = \frac{\sqrt{[s_{12} - (m_{1} - m_{2})^{2}] [s_{12} - (m_{1} + m_{2})^{2}]}}{2 \sqrt{s_{12}}}.
\end{equation}
Thus, in terms of the masses, you find the energies:
\begin{align}
	E_{1} = \frac{\sqrt{4 m_{1}^{2} s_{12} + F_{\red{123}}}}{2 \sqrt{s_{12}}} = \frac{s_{12} - m_{2}^{2} + m_{1}^{2}}{2 \sqrt{s_{12}}}, \\
	E_{2} = \frac{\sqrt{4 m_{2}^{2} s_{12} + F_{\red{123}}}}{2 \sqrt{s_{12}}} = \frac{s_{12} - m_{1}^{2} + m_{2}^{2}}{2 \sqrt{s_{12}}}.
\end{align}
With the energies and spatial momentum, you can find the speed of each quantum,
\begin{align}
	\vert \mathbf{v}_{1} \vert &= \frac{\sqrt{F_{\red{123}}}}{s_{12} - m_{2}^{2} + m_{1}^{2}} = \frac{\sqrt{[s_{12} - (m_{1} - m_{2})^{2}] [s_{12} - (m_{1} + m_{2})^{2}]}}{s_{12} - m_{2}^{2} + m_{1}^{2}} , \\
	\vert \mathbf{v}_{2} \vert &= \frac{\sqrt{F_{\red{123}}}}{s_{12} - m_{1}^{2} + m_{2}^{2}} = \frac{\sqrt{[s_{12} - (m_{1} - m_{2})^{2}] [s_{12} - (m_{1} + m_{2})^{2}]}}{s_{12} - m_{1}^{2} + m_{2}^{2}} , \\
	\vert \mathbf{v}_{3} \vert &= 0 .
\end{align}
Note that the two nonzero speeds can be written as functions of two dimensionless ratios:
\begin{equation}
	\frac{s_{12}}{m_{1}m_{2}}, \quad \frac{m_{1}}{m_{2}}.
\end{equation}
With the speeds, you can find the rapidities,
\begin{align}
	\eta_{1} &= \frac{1}{2} \log{\left[ \frac{(m_{1} - m_{2})(m_{1} + m_{2}) + s_{12} + \sqrt{F_{\red{123}}}}{(m_{1} - m_{2})(m_{1} + m_{2}) + s_{12} - \sqrt{F_{\red{123}}}} \right]} , \\
	\eta_{2} &= \frac{1}{2} \log{\left[ \frac{(m_{1} - m_{2})(m_{1} + m_{2}) - s_{12} - \sqrt{F_{\red{123}}}}{(m_{1} - m_{2})(m_{1} + m_{2}) - s_{12} + \sqrt{F_{\red{123}}}} \right]} , \\
	\eta_{3} &= 0.
\end{align}
The sum of the two incoming rapidities is important:
\begin{equation}
	\eta_{1} + \eta_{2} = \frac{1}{2} \log{\left[ \frac{m_{1}^{2} + m_{2}^{2} - s_{12} - \sqrt{F_{\red{123}}}}{m_{1}^{2} + m_{2}^{2} - s_{12} + \sqrt{F_{\red{123}}}} \right]} = \log{\left[ \frac{(m_{1} + m_{2})^{2} - s_{12} + \sqrt{F_{\red{123}}}}{(m_{1} + m_{2})^{2} - s_{12} - \sqrt{F_{\red{123}}}} \right]} .
\end{equation}
This kind of logarithmic expression appears in loop-level amplitudes for 4-point elastic processes.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Physical Scattering Region}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Identical Massive Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Massless Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...