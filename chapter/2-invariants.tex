%Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí
\chapter{Kinematic Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
This chapter contains an overview of different kinematic invariants.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Energy-Momentum Vectors}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
I will work in $D$ spacetime dimensions. This means that every energy-momentum vector has one energy component and $D-1$ spatial momentum components. In a scattering process with $M$ incoming quanta and $N$ outgoing quanta there are a total of $(M + N)$ energy variables and $(M + N) (D - 1)$ spatial momentum components. The scattering amplitude is a function of the energy-momentum vectors, so it is really a function of $(M + N)D$ variables. However, if there is a symmetry invariance, then the number of independent variables is much smaller.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Translation Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In $D$ spacetime dimensions there are $D$ independent translations. Thus, a translation-invariant scattering amplitude for a process with $M$ incoming and $N$ outgoing off-shell quanta is a function of
\begin{equation}
	\operatorname{Tran}(M, N, D) = (M + N - 1)D
\end{equation}
variables. This agrees with the expectation from the conservation of momentum: the conservation constraint allows you to write one of the energy-momentum vectors in terms of the other $M + N - 1$ vectors.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Lorentz Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In $D$ spacetime dimensions there are $D(D - 1) / 2$ independent Lorentz transformations. Thus, a Lorentz-invariant scattering amplitude for a process with $M$ incoming and $N$ outgoing off-shell quanta is a function of
\begin{equation}
	\operatorname{Lore}(M, N, D) = (M + N)D - \frac{D (D - 1)}{2}
\end{equation}
variables.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Poincar\'{e} Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Poincar\'{e} transformations include both translations and Lorentz transformations. Thus, a Poincar\'{e}-invariant scattering amplitude for a process with $M$ incoming and $N$ outgoing off-shell quanta is a function of
\begin{equation}
	\operatorname{Poin}(M, N, D) = (M + N) D - \frac{D (D + 1)}{2}
\end{equation}
variables. Setting $n = M + N$ and $D = 4$ leads to
\begin{equation}
	\operatorname{Poin}(n, 4) = 4n - 10.
\end{equation}
Usually the $n$ external energy-momentum vectors satisfy an on-shell constraint, so you really have $\operatorname{Poin}(n, 4) - n = 3n - 10$ independent variables which is positive when $n > 3$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Conformal Invariance}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Conformal transformations include one dilatation, $D$ conformal boosts, and the Poincar\'{e} transformations. Thus, a conformal-invariant scattering amplitude for a process with $M$ incoming and $N$ outgoing off-shell quanta is a function of
\begin{equation}
	\operatorname{Conf}(M, N, D) = (M + N)D - \frac{(D + 2) (D + 1)}{2}
\end{equation}
variables. Setting $n = M + N$ and $D = 4$ leads to
\begin{equation}
	\operatorname{Conf}(n, 4) = 4n - 15.
\end{equation}
Usually the $n$ external energy-momentum vectors satisfy an on-shell constraint, so you really have $\operatorname{Conf}(n, 4) - n = 3(n - 5)$ independent variables which is positive when $n > 5$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Threshold and Pseudothreshold Values}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
When working with external massive quanta, you find that particular linear combinations of the masses appear often. Given $n + 1$ distinct masses, you can construct a single \textbf{threshold value} and a set of $2^{n} - 1$ \textbf{pseudothreshold values}. The threshold value is always given by the square of the sum of the masses, while the pseudothreshold values are found by taking the square of a combination of sums and differences of masses. Just like for Mandelstam invariants below, I will refer to these values as the $(n + 1)$-threshold and $(n + 1)$-pseudothreshold values. Taking the average over the $(n + 1)$-threshold value and the $2^{n} - 1$ $(n + 1)$-pseudothreshold values gives the sum of squares of the $n + 1$ masses.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Two Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given two distinct masses $\red{m}$, and $\blue{m}$, the \textbf{2-threshold value} is
\begin{equation}
	(\red{m} + \blue{m})^{2},
\end{equation}
and the \textbf{2-pseudothreshold value} is
\begin{equation}
	(\red{m} - \blue{m})^{2}.
\end{equation}
In the limit $\blue{m} \rightarrow 0$ the 2-threshold and 2-pseudothreshold become the same.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Three Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given three distinct masses $\red{m}$, $\blue{m}$, and $\green{m}$, the \textbf{3-threshold value} is
\begin{equation}
	(\red{m} + \blue{m} + \green{m})^{2},
\end{equation}
and the three \textbf{3-pseudothreshold values} are
\begin{equation}
	(\red{m} - \blue{m} - \green{m})^{2}, \quad (\red{m} - \blue{m} + \green{m})^{2}, \quad (\red{m} + \blue{m} - \green{m})^{2}.
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Four Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given four distinct masses $\cyan{m}$, $\magenta{m}$, $\yellow{m}$, and $m$ the \textbf{4-threshold value} is
\begin{equation}
	(\cyan{m} + \magenta{m} + \yellow{m} + m)^{2},
\end{equation}
and the seven \textbf{4-pseudothreshold values} are
\begin{equation}
\begin{split}
	(\cyan{m} + \magenta{m} - \yellow{m} - m)^{2}, \quad (\cyan{m} - \magenta{m} + \yellow{m} - m)^{2}, \quad (\cyan{m} - \magenta{m} - \yellow{m} + m)^{2}, \\
	(\cyan{m} - \magenta{m} - \yellow{m} - m)^{2}, \quad (\cyan{m} - \magenta{m} + \yellow{m} + m)^{2}, \\
	(\cyan{m} + \magenta{m} - \yellow{m} + m)^{2}, \quad (\cyan{m} + \magenta{m} + \yellow{m} - m)^{2}.
\end{split}
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mandelstam Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The Mandelstam momentum invariants are Lorentz scalar quantities that are made with linear combinations of energy-momentum vectors from different external quanta. The particular linear combination depends on whether the energy-momentum vectors are adjacent or nonadjacent.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Two Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given two energy-momentum vectors $p_{I}$ and $p_{J}$, the \textbf{adjacent 2-Mandelstam} invariant is given by
\begin{equation}
	s_{IJ} \equiv (p_{I} + p_{J})^{2},
\end{equation}
and the \textbf{nonadjacent 2-Mandelstam} invariant is given by
\begin{equation}
	t_{IJ} \equiv (p_{I} - p_{J})^{2}.
\end{equation}
Note that
\begin{equation}
	s_{IJ} - m_{I}^{2} - m_{J}^{2} = m_{I}^{2} + m_{J}^{2} - t_{IJ},
\end{equation}
so in practice you either work with $s_{IJ}$ or $t_{IJ}$. Since
\begin{equation}
	t_{IJ} = 2m_{I}^{2} + 2m_{J}^{2} - s_{IJ},
\end{equation}
it follows that
\begin{align}
	s_{IJ} < (m_{I} - m_{J})^{2} \quad &\Longrightarrow \quad t_{IJ} > (m_{I} + m_{J})^{2}, \\
	s_{IJ} = (m_{I} - m_{J})^{2} \quad &\Longrightarrow \quad t_{IJ} = (m_{I} + m_{J})^{2}, \\
	(m_{I} - m_{J})^{2} < s_{IJ} < m_{I}^{2} + m_{J}^{2} \quad &\Longrightarrow \quad m_{I}^{2} + m_{J}^{2} < t_{IJ} < (m_{I} + m_{J})^{2}, \\
	s_{IJ} = m_{I}^{2} + m_{J}^{2} \quad &\Longrightarrow \quad t_{IJ} = m_{I}^{2} + m_{J}^{2}, \\
	m_{I}^{2} + m_{J}^{2} < s_{IJ} < (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad (m_{I} - m_{J})^{2} < t_{IJ} < m_{I}^{2} + m_{J}^{2}, \\
	s_{IJ} = (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad t_{IJ} = (m_{I} - m_{J})^{2}, \\
	s_{IJ} > (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad t_{IJ} < (m_{I} - m_{J})^{2}.
\end{align}

With $N$ energy-momentum vectors you can make a total of
\begin{equation}
	{N \choose 2} = \frac{N(N-1)}{2}
\end{equation}
possible 2-Mandelstam invariants. However, if there is a conservation constraint, then there are $N$ linear relations that reduce the number of independent invariants to be
\begin{equation}
	\mathfrak{M}_{2}(N) = \frac{N(N-1)}{2} - N = \frac{N(N-3)}{2}, \quad N > 4.
\end{equation}
Some particular values of $\mathfrak{M}_{2}(N)$ listed in Table \ref{tablM2}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\centering
\begin{tabular}{|c|cccccc|}
\hline
$N$ & 3 & 4 & 5 & 6 & 7 & 8 \\ \hline
$\mathfrak{M}_{2}(N)$ & 0 & 2 & 5 & 9 & 14 & 20 \\ 
$\mathfrak{M}_{3}(N)$ & 0 & 0 & 0 & 10 & 35 & 56 \\
$\mathfrak{M}_{4}(N)$ & 0 & 0 & 0 & 0 & 0 & 35 \\ \hline
\end{tabular}
\caption{Values of $\mathfrak{M}_{n}(N)$ for $n = 2,3,4$.}
\label{tablM2}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Three Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Now consider three energy-momentum vectors $p_{I}$, $p_{J}$, and $p_{K}$. The \textbf{adjacent 3-Mandelstam} invariant is given by
\begin{equation}
	s_{IJK} \equiv (p_{I} + p_{J} + p_{K})^{2}.
\end{equation}
Now there are three types of \textbf{nonadjacent 3-Mandelstam} invariants:
\begin{align}
	a_{IJK} &\equiv (p_{I} - p_{J} - p_{K})^{2}, \\
	b_{IJK} &\equiv (p_{I} - p_{J} + p_{K})^{2}, \\
	c_{IJK} &\equiv (p_{I} + p_{J} - p_{K})^{2}.
\end{align}
Any 3-Mandelstam invariant can always be written in terms of 2-Mandelstam invariants and masses:
\begin{align}
	s_{IJK} &= s_{IJ} + s_{IK} + s_{JK} - m_{I}^{2} - m_{J}^{2} - m_{K}^{2} ,\\
	a_{IJK} &= t_{IJ} + t_{IK} + s_{JK} - m_{I}^{2} - m_{J}^{2} - m_{K}^{2} ,\\
	b_{IJK} &= t_{IJ} + s_{IK} + t_{JK} - m_{I}^{2} - m_{J}^{2} - m_{K}^{2} ,\\
	c_{IJK} &= s_{IJ} + t_{IK} + t_{JK} - m_{I}^{2} - m_{J}^{2} - m_{K}^{2} .
\end{align}
Note that
\begin{equation}
	s_{IJK} + a_{IJK} + b_{IJK} + c_{IJK} = 4m_{I}^{2} + 4m_{J}^{2} + 4m_{K}^{2}.
\end{equation}
With $N$ energy-momentum vectors, you can make a total of
\begin{equation}
	\mathfrak{M}_{3}(N) = {N \choose 3} = \frac{N(N-1)(N-2)}{6}, \quad N > 6,
\end{equation}
possible 3-Mandelstam invariants. If there is a conservation constraint, then $\mathfrak{M}_{3}(6) = 10$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Four Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Next, consider four energy-momentum vectors $p_{I}$, $p_{J}$, $p_{K}$, and $p_{L}$. Now there are six adjacency pairs. This leads to three adjacency possibilities. If all six pairs are adjacent, then the \textbf{4-Mandelstam} invariant is given by
\begin{equation}
	s_{IJKL} \equiv (p_{I} + p_{J} + p_{K} + p_{L})^{2}.
\end{equation}
Another possibility is that only three pairs are adjacent. Then the \textbf{4-Mandelstam} invariant is given by one of four possibilities:
\begin{align}
	t_{IJKL}{}^{(1)} &\equiv (p_{I} - p_{J} - p_{K} - p_{L})^{2}, \\
	t_{IJKL}{}^{(2)} &\equiv (p_{I} - p_{J} + p_{K} + p_{L})^{2}, \\
	t_{IJKL}{}^{(3)} &\equiv (p_{I} + p_{J} - p_{K} + p_{L})^{2}, \\
	t_{IJKL}{}^{(4)} &\equiv (p_{I} + p_{J} + p_{K} - p_{L})^{2}.
\end{align}
The last possibility is that only two pairs are adjacent. Then the \textbf{4-Mandelstam} invariant is given by one of three possibilities:
\begin{align}
	u_{IJKL}{}^{(1)} &\equiv (p_{I} + p_{J} - p_{K} - p_{L})^{2}, \\
	u_{IJKL}{}^{(2)} &\equiv (p_{I} - p_{J} + p_{K} - p_{L})^{2}, \\
	u_{IJKL}{}^{(3)} &\equiv (p_{I} - p_{J} - p_{K} + p_{L})^{2}.	
\end{align}
Just like a 3-Mandelstam invariant, a 4-Mandelstam invariant can always be written in terms of 2-Mandelstam invariants and masses. For example,
\begin{equation}
	s_{IJKL} = s_{IJ} + s_{IK} + s_{IL} + s_{JK} + s_{JL} + s_{KL} - 2m_{I}^{2} - 2m_{J}^{2} - 2m_{K}^{2} - 2m_{L}^{2}.
\end{equation}
With $N$ energy-momentum vectors, you can make a total of
\begin{equation}
	\mathfrak{M}_{4}(N) = {N \choose 4} = \frac{N(N-1)(N-2)(N-3)}{24}, \quad N > 8,
\end{equation}
possible 4-Mandelstam invariants. If there is a conservation constraint, then $\mathfrak{M}_{4}(8) = 35$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Regge Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
At this point it is not clear why you would bother working with $n$-Mandelstam invariants for $n > 2$ since these invariants can always be written in terms of 2-Mandelstam invariants and masses. In order to define a 2-Mandelstam invariant you need two distinct energy-momentum vectors. If both of these describe slow quanta, then one can also define what I call a Regge invariant. If the pair of quanta is adjacent, then the \textbf{Regge invariant} is given by
\begin{equation}
	\sigma_{IJ} \equiv \frac{p_{I} \cdot p_{J}}{\sqrt{p_{I}^{2}} \sqrt{p_{J}^{2}}} = \frac{s_{IJ} - m_{I}^{2} - m_{J}^{2}}{2m_{I} m_{J}}.
\end{equation}
Otherwise the pair of quanta is nonadjacent and the \textbf{Regge invariant} is given by
\begin{equation}
	\tau_{IJ} \equiv -\frac{p_{I} \cdot p_{J}}{\sqrt{p_{I}^{2}} \sqrt{p_{J}^{2}}} = \frac{t_{IJ} - m_{I}^{2} - m_{J}^{2}}{2m_{I} m_{J}}.
\end{equation}
In contrast with the 2-Mandelstam invariants, the Regge invariants are dimensionless. Indeed, they are functions of two dimensionless ratios:
\begin{equation}
	\sigma_{IJ}\left( \frac{s_{IJ}}{m_{I} m_{J}}, \frac{m_{I}}{m_{J}} \right), \quad \tau_{IJ}\left( \frac{t_{IJ}}{m_{I} m_{J}}, \frac{m_{I}}{m_{J}} \right).
\end{equation}
Note that
\begin{align}
	s_{IJ} < (m_{I} - m_{J})^{2} \quad &\Longrightarrow \quad \sigma_{IJ} < -1, \\
	s_{IJ} = (m_{I} - m_{J})^{2} \quad &\Longrightarrow \quad \sigma_{IJ} = -1, \\
	(m_{I} - m_{J})^{2} < s_{IJ} < m_{I}^{2} + m_{J}^{2} \quad &\Longrightarrow \quad -1 < \sigma_{IJ} < 0, \\
	s_{IJ} = m_{I}^{2} + m_{J}^{2} \quad &\Longrightarrow \quad \sigma_{IJ} = 0, \\
	m_{I}^{2} + m_{J}^{2} < s_{IJ} < (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad 0 < \sigma_{IJ} < 1, \\
	s_{IJ} = (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad \sigma_{IJ} = 1, \\
	s_{IJ} > (m_{I} + m_{J})^{2} \quad &\Longrightarrow \quad \sigma_{IJ} > 1.
\end{align}
Each of these regions describes a different kinematic regime. Regge invariants appear in Regge trajectory functions.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Gram Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
You can construct $n$-Mandelstam invariants with a collection of $n$ distinct energy-momentum vectors. With such a collection you can also construct $n$-Gram invariants:
\begin{equation}
	G_{I_{1} \cdots I_{n}} \equiv - \det{\begin{pmatrix}
	p_{I_{1}} \cdot p_{I_{1}} & \cdots & p_{I_{1}} \cdot p_{I_{n}} \\
	\vdots & \ddots & \vdots \\
	p_{I_{1}} \cdot p_{I_{n}} & \cdots & p_{I_{n}} \cdot p_{I_{n}}
	\end{pmatrix}}.
\end{equation}
A $n$-Gram invariant is the negative of the Gram determinant associated with the collection of $n$ distinct energy-momentum vectors. By construction the Gram momentum invariants are sensitive to collinearity (e.g. they vanish whenever a pair of vectors is collinear).

In a space with euclidean signature, the square root of the $n$-Gram determinant has a geometric interpretation: the euclidean $n$-volume of a euclidean $n$-parallelotope. Thus, in euclidean space you expect:
\begin{equation}
	G_{I_{1} \cdots I_{n}} \leq 0.
\end{equation}
Since the energy-momentum vectors live in Minkowski spacetime, the Gram momentum invariants do not have a euclidean geometric interpretation. Thus, in Minkowski spacetime you can have both $G_{I_{1} \cdots I_{n}} < 0$ and $G_{I_{1} \cdots I_{n}} > 0$ have physical meaning.

% Another way to motivate the $n$-Gram invariants is by introducing energy-momentum $n$-blades using the wedge product:
% \begin{equation}
% 	G_{I_{1} \cdots I_{n}} = - \vert p_{I_{1}} \wedge p_{I_{2}} \wedge \cdots \wedge p_{I_{n}} \vert
% \end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Two Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given two linearly-independent energy-momentum vectors, the \textbf{2-Gram} invariant is given by
\begin{equation}
	G_{IJ} \equiv - \det{ \begin{pmatrix}
	p_{I}^{2} & p_{I} \cdot p_{J} \\
	p_{I} \cdot p_{J} & p_{J}^{2}
\end{pmatrix}	 } = -p_{I}^{2} p_{J}^{2} + (p_{I} \cdot p_{J})^{2}.
\end{equation}
If the pair of quanta is adjacent, then $G_{IJ}$ can be written as
\begin{equation}
	G_{IJ} = \frac{1}{4} [s_{IJ} - (m_{I} - m_{J})^{2}] [s_{IJ} - (m_{I} + m_{J})^{2}].
\end{equation}
Otherwise, the pair of quanta is nonadjacent, and the 2-Gram invariant can be written as
\begin{equation}
	G_{IJ} = \frac{1}{4} [t_{IJ} - (m_{I} - m_{J})^{2}] [t_{IJ} - (m_{I} + m_{J})^{2}].
\end{equation}
In this form the sign properties of $G_{IJ}$ are more transparent:
\begin{align}
	G_{IJ} < 0 \quad &\Longrightarrow \quad (m_{I} - m_{J})^{2} < s_{IJ} < (m_{I} + m_{J})^{2}, \\
	G_{IJ} = 0 \quad &\Longrightarrow \quad s_{IJ} = (m_{I} - m_{J})^{2} \text{ or } s_{IJ} = (m_{I} + m_{J})^{2}, \\
	G_{IJ} > 0 \quad &\Longrightarrow \quad s_{IJ} < (m_{I} - m_{J})^{2} \text{ or } s_{IJ} > (m_{I} + m_{J})^{2}.
\end{align}
The semi-infinite regions when $G_{IJ} > 0$ are associated with scattering.

Expanding $G_{IJ}$ explicitly you find a surprisingly symmetric expression,
\begin{equation}
	G_{IJ} = \frac{1}{4} \left( s_{IJ}^{2} + m_{I}^{4} + m_{J}^{4} -2 m_{I}^{2} s_{IJ} - 2 m_{J}^{2} s_{IJ} - 2 m_{I}^{2}m_{J}^{2} \right),
\end{equation}
which is sometimes rewritten in terms of a multivariate polynomial:
\begin{equation}
	G_{IJ} = \frac{1}{4} \Upsilon(s_{IJ}, m_{I}^{2}, m_{J}^{2}), \quad \Upsilon(x, y, z) \equiv x^{2} + y^{2} + z^{2} - 2 xy - 2 xz - 2 yz.
	\label{Kallen}
\end{equation}
The polynomial $\Upsilon$ is a \textbf{K\"{a}ll\'{e}n polynomial}. Note that $\Upsilon$ is homogeneous. Due to the symmetry properties of $\Upsilon$, you can write it in many equivalent ways:
\begin{equation}
\begin{split}
	\Upsilon(x, y, z) &= (\sqrt{x} + \sqrt{y} + \sqrt{z})(\sqrt{x} - \sqrt{y} - \sqrt{z})(\sqrt{x} - \sqrt{y} + \sqrt{z})(\sqrt{x} + \sqrt{y} - \sqrt{z}) \\
	&= [x - (\sqrt{y} + \sqrt{z})^{2}] [x - (\sqrt{y} - \sqrt{z})^{2}] \\
	&= [y - (\sqrt{x} + \sqrt{z})^{2}] [y - (\sqrt{x} - \sqrt{z})^{2}] \\
	&= [z - (\sqrt{x} + \sqrt{y})^{2}] [z - (\sqrt{x} - \sqrt{y})^{2}].
\end{split}
\end{equation}
The polynomial $\Upsilon$ appears in the expression for the euclidean area $A$ of a euclidean triangle with sides $x$, $y$, and $z$:
\begin{equation}
	A = \frac{1}{4} \sqrt{-\Upsilon(x, y, z)}.
\end{equation}
This is known as Heron's formula. Without loss of generality, I will assume that
\begin{equation}
	x \leq y \leq z.
\end{equation}

You can use the Regge invariant to write the 2-Gram invariant as
\begin{equation}
	G_{IJ} = m_{I}^{2} m_{J}^{2} \left( \sigma_{IJ} + 1 \right)\left( \sigma_{IJ} - 1 \right) = m_{I}^{2} m_{J}^{2} \left( \sigma_{IJ}^{2} - 1 \right) \equiv m_{I}^{2} m_{J}^{2} R_{2}(\sigma).
\end{equation}
Here $R_{2}$ is a \textbf{Regge polynomial}. Note that $R_{2}$ is not homogeneous.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Three Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given three linearly-independent energy-momentum vectors, the \textbf{3-Gram} invariant is given by
\begin{equation}
\begin{split}
	G_{IJK} \equiv &{}- \det{ \begin{pmatrix}
	p_{I}^{2} & p_{I} \cdot p_{J} & p_{I} \cdot p_{K} \\
	p_{I} \cdot p_{J} & p_{J}^{2} & p_{J} \cdot p_{K} \\
	p_{I} \cdot p_{K} & p_{J} \cdot p_{K} & p_{K}^{2}
\end{pmatrix}} \\
	= &{}-p_{I}^{2} p_{J}^{2} p_{K}^{2} - 2 (p_{I} \cdot p_{J}) (p_{I} \cdot p_{K}) (p_{J} \cdot p_{K}) \\
	&{}+ p_{I}^{2} (p_{J} \cdot p_{K})^{2} + p_{J}^{2} (p_{I} \cdot p_{K})^{2} + p_{K}^{2} (p_{I} \cdot p_{J})^{2}.
\end{split}
\end{equation}
Without much loss of generality, I will assume that all three pairs of quanta are adjacent. Then, in terms of the three Regge invariants, you can write
\begin{equation}
	G_{IJK} = m_{I}^{2} m_{J}^{2} m_{K}^{2} \left(2 \sigma_{IJ} \sigma_{IK} \sigma_{JK} - \sigma_{IJ}^{2} - \sigma_{IK}^{2} - \sigma_{JK}^{2} + 1 \right) \equiv m_{I}^{2} m_{J}^{2} m_{K}^{2} R_{3}(\sigma).
\end{equation}
Although there is a trilinear term, the polynomial $R_{3}$ is at worst quadratic in a given Regge invariant.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Four Quanta}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dual Spacetime}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Every $M$-to-$N$ scattering process is assumed to conserve external energy-momentum. This is enforced by the conservation constraint:
\begin{equation}
	p_{1} + p_{2} + \ldots + p_{M} = q_{1} + q_{2} + \ldots + q_{N}.
\end{equation}
This constraint can be solved explicitly by introducing $M + N$ \textbf{dual spacetime coordinates}:
\begin{align}
	p_{1} = d_{2} - d_{1}, \quad p_{2} &= d_{3} - d_{2}, \quad \ldots \quad p_{M} = d_{M+1} - d_{M}, \\
	q_{1} = d_{M+1} - d_{M+2}, \quad q_{2} &= d_{M+2} - d_{M+3}, \quad \ldots \quad q_{N} = d_{M+N} - d_{1}.
\end{align}
In essence, this is just a change of basis. However, this change of basis leads to a different way of thinking about kinematics. By introducing coordinates $d_{I}$ for points in an auxiliary dual spacetime, you are allowed to invoke many concepts from distance geometry.

Since an energy-momentum vector becomes a dual coordinate interval, the $M+N$ external energy-momentum vectors define a \textbf{skew polygon} that lives in the dual spacetime. Given a collection of $n$ points, you can draw a total of $n$-choose-2 lines that connect pairs of points. All of these lines define a complete graph with $n$ vertices and $n(n-1)/2$ edges. You can designate $n$ of these edges as \textbf{external edges} and the other $n(n-3)/2$ as \textbf{internal edges}. The external edges correspond to the external energy-momentum vectors. Thus, the length of an external edge is related to the mass of an external quantum. (Distance in the dual spacetime has units of mass.) The internal edges correspond to particular linear combinations of energy-momentum vectors that can be used to construct the set of \textbf{polygonal Mandelstam invariants}. This set of Mandelstam invariants contains the maximum number of linearly-independent Mandelstam invariants. Note that the polygonal invariants include other types of Mandelstam invariants besides 2-Mandelstam invariants.

A scattering process does not have a unique dual polygon associated to it. This is so because the way in which the dual coordinates are introduced is not unique. For example, the dual polygon made with the sequence
\begin{equation}
	p_{1} \rightarrow p_{2} \rightarrow -q_{1} \rightarrow -q_{2} \rightarrow p_{1}
	\label{poly1}
\end{equation}
is different from the dual polygon made with the sequence
\begin{equation}
	p_{2} \rightarrow p_{1} \rightarrow -q_{1} \rightarrow -q_{2} \rightarrow p_{2}.
	\label{poly2}
\end{equation}
Here $p_{2}$ and $-q_{1}$ are neighbors in (\ref{poly1}), but not in (\ref{poly2}). That is, the \textbf{sequence contiguity} is different. For a polygon with $n$ edges, you could expect to have $n!$ distinct dual polygons. However, a pair of polygons that is related by a cyclic rotation or by a reflection will have the same set of polygonal Mandelstam invariants because each of these transformations does not change the sequence contiguity. Thus, the number of \textbf{inequivalent sets} of polygonal Mandelstam invariants is
\begin{equation}
	\mathfrak{P}_{n} \equiv \frac{n!}{2n} = \frac{(n - 1)!}{2}.
\end{equation}
In some ways, $\mathfrak{P}_{n}$ counts the number of \textbf{inequivalent planar classes} of Feynman graphs. However, depending on the external content of the scattering process, and on the dynamics, some of these classes might not be planar. Some values of $\mathfrak{P}_{n}$ are given in Table \ref{tablPn}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{table}
\centering
\begin{tabular}{|c|cccccc|}
\hline
$n$ & 3 & 4 & 5 & 6 & 7 & 8 \\ \hline
$\mathfrak{P}_{n}$ & 1 & 3 & 12 & 60 & 360 & 2520 \\ \hline
\end{tabular}
\caption{Values of $\mathfrak{P}_{n}$.}
\label{tablPn}
\end{table}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Simplex Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The minkowskian $n$-Gram invariants have a geometric interpretation: they are related to the minkowskian $n$-volume of an $n$-parallelotope, which is naturally constructed from a collection of vectors. In the dual spacetime, you have the coordinates of points instead. Given a collection of $n$ points in Minkowski spacetime, you can consider the minkowskian $(n-1)$-simplex that contains these points as vertices. The minkowskian $(n-1)$-volume of such an $(n-1)$-simplex can be obtained from the $n$-point Cayley-Menger determinant:
\begin{equation}
	\det{ \begin{pmatrix}
	0 & d_{12}^{2} & d_{13}^{2} & \cdots & d_{1n}^{2} & 1 \\
	d_{12}^{2} & 0 & d_{23}^{2} & \cdots & d_{2n}^{2} & 1 \\
	d_{13}^{2} & d_{23}^{2} & 0 & \cdots & d_{3n}^{2} & 1 \\
	\vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
	d_{1n}^{2} & d_{2n}^{2} & d_{3n}^{2} & \cdots & 0 & 1 \\
	1 & 1 & 1 & \cdots & 1 & 0
\end{pmatrix}	 }, \quad d_{IJ} \equiv d_{I} - d_{J}.
\end{equation}
However, since the $n$-volume of an $n$-simplex is a fraction of the $n$-volume of an $n$-parallelotope, at first glance it is not obvious why one should compute these invariants too. Given a collection of $n$ energy-momentum vectors satisfying a conservation constraint, you can construct nontrivial 2-parallelotopes, 3-parallelotopes, ... all the way to $(n-1)$-parallelotopes. But this feels somewhat fragmented. In contrast, after changing basis to the dual spacetime coordinates, you naturally have an $(n-1)$-simplex and then you can consider its different $k$-faces. It is true that the $k$-volume of some of these $k$-faces is related to one of the $k$-Gram invariants, but you also get $k$-faces that are not related to any $k$-Gram invariant. Indeed, there is a unique $(n-1)$-simplex for each inequivalent contiguity.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dual Conformal Invariants}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The dual coordinate intervals $d_{I} - d_{J}$ allows you to build \textbf{dual Poincar\'{e} invariants}:
\begin{equation}
	(d_{I} - d_{J})^{2}.
\end{equation}
The set of dual Poincar\'{e} invariants is the set of polygonal Mandelstam invariants and the masses. Given the dual coordinates of four distinct points in the dual spacetime, one can construct a \textbf{dual conformal ratio}:
\begin{equation}
	[I, J; K, L] \equiv \frac{(d_{I} - d_{K})^{2} (d_{J} - d_{L})^{2}}{(d_{I} - d_{L})^{2} (d_{J} - d_{K})^{2}}.
\end{equation}
The dual conformal ratio is invariant under dual conformal transformations.

There are $4! = 24$ possible permutations of the four coordinates, so you could expect 24 different values for the dual conformal ratio. However, swapping the coordinates pair-wise leaves $[I, J; K, L]$ invariant:
\begin{equation}
	[I, J; K, L] = [J, I; L, K] = [K, L; I, J] = [L, K; J, I].
\end{equation}
This means that out of the 24 possible values, only six are different:
\begin{equation}
\begin{split}
	[I, J; K, L] = u, \quad [I, K; J, L] &= v, \quad [I, L; K, J] = \frac{u}{v}, \\
	[I, J; L, K] = \frac{1}{u}, \quad [I, K; L, J] &= \frac{1}{v}, \quad [I, L; J, K] = \frac{v}{u}.
\end{split}
\end{equation}
If $u$ and $v$ are given, then all six different values are known. Thus, for a given quartet of coordinates only two values are independent. Note that each of the six different values can be written in the form:
\begin{equation}
	\frac{a_{u} u + a_{v} v + b}{c_{u} u + c_{v} v + d},
\end{equation}
which looks like a generalized M\"{o}bius transformation.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Kinematic Polytopes}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In general, an $n$-point scattering process has an associated set of inequivalent $(n - 1)$-simplices in the dual spacetime.

For an $(N+1)$-point scattering process with $N$ incoming quanta and one outgoing quantum the associated $N$-simplex is an example of an $N$-corporatope. The \textbf{corporatopes} are the dual spacetime simplices associated to a many-body system.

For a maximally-diverse elastic $2N$-point scattering process the associated $(2N - 1)$-simplex is an example of an \textbf{elastitope}.

In the $N$-body semiforward regime, you reduce a $(2N - 1)$-elastitope to an $N$-corporatope. That is, the $N$-body semiforward regime takes a scattering amplitude for a $2N$-point process and yields a scattering amplitude for an $(N+1)$-point process.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Speed and Rapidity}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
A slow relativistic quantum with mass $m$, spatial momentum vector $\mathbf{p}$ and energy $E$ has a \textbf{speed} $\vert \mathbf{v} \vert$ given by
\begin{equation}
	\vert \mathbf{v} \vert = \frac{\sqrt{E^{2} - m^{2}}}{E} = \frac{\vert \mathbf{p} \vert}{\sqrt{m^{2} + \mathbf{p}^{2}}} = \frac{\vert \mathbf{p} \vert}{E}.
\end{equation}
If the quantum is physical, then $ E \geq m $ or, equivalently, $ \vert \mathbf{p} \vert < E $. Both of these conditions lead to a bound on the speed:
\begin{equation}
	0 \leq \vert \mathbf{v} \vert < 1.
\end{equation}
In contrast with energy, mass or spatial momentum, speed and velocity are dimensionless. However, energy and spatial momentum are conserved, but velocity is not. Given the speed, you can find the quantum's \textbf{rapidity} $\eta$ via the relation
\begin{equation}
	\eta = \operatorname{arctanh}{\vert \mathbf{v} \vert} = \frac{1}{2} \log{\left( \frac{1 + \vert \mathbf{v} \vert}{1 - \vert \mathbf{v} \vert} \right)}.
\end{equation}
Since the speed of a physical quantum is bounded, it follows that the rapidity of a physical quantum is bounded from below: $0 \leq \eta < \infty$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Scattering Angles}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
...