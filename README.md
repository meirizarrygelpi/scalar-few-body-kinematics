# Scalar Few-Body Kinematics

This is a set of notes on the kinematics of few-body processes with scalar quanta.

Copyright (c) 2015 Melvin Eloy Irizarry-Gelpí